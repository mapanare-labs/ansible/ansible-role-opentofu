# Ansible Role: OpenTofu

[![pipeline status](https://gitlab.com/mapanare-labs/ansible/ansible-role-opentofu/badges/main/pipeline.svg)](https://gitlab.com/mapanare-labs/ansible/ansible-role-opentofu/-/commits/main)

This role installs [OpenTofu](https://www.opentofu.org/) application on any supported host.

## Requirements

None.

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/mapanare-labs/ansible/ansible-role-opentofu enmanuelmoreira.opentofu
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    opentofu_package_name: opentofu
    opentofu_package_state: present

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: true
      roles:
        - role: enmanuelmoreira.opentofu

## License

MIT / BSD
